#!/usr/bin/bash
MINUTES="+${1} minutes"
DATESTOP=$(date -d "${MINUTES}" +%s)
DATENOW=$(date +%s)

echo "Tiempo final: $(date -d "${MINUTES}" +%X)"

while (( ${DATESTOP}>${DATENOW} )) ; do
  DATENOW=$(date +%s)
  echo -ne "\rTiempo actual: $(date +%X)"
  sleep 10
done
